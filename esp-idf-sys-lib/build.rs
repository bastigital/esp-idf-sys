extern crate esp_buster as buster;
use std::env;
use std::fs::copy;
use std::path::{Path, PathBuf};
use std::fs::{read_dir, write, create_dir, create_dir_all, File};
use std::io::{self, BufRead};
use std::ffi::OsString;
use cmake::build;
use std::process::Command;
use buster::config::Lib::*;
use buster::idf::{BuilderConfig, BuilderLink, BuilderLoad};

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

macro_rules! svec {
    ($($x:expr),*) => (vec![$($x.to_string()),*]);
}



fn main() {

    let out_dir = &PathBuf::from(env::var("OUT_DIR").unwrap());
    let manifest_dir = &PathBuf::from(env::var("CARGO_MANIFEST_DIR").unwrap());
    let idf_path = PathBuf::from(env::var("IDF_PATH").unwrap());
    let include_base = idf_path.join("components");
    let libs = read_lines(manifest_dir.join("lib").join("build").join("ldgen_libraries"));
    let mut includes = "".to_string();

    let mut b = buster::idf::Builder::new();
    let b = b
        .idf_path(idf_path)
        .build_path(manifest_dir.join("lib").join("build"))
        .out(out_dir)
        .linker("xtensa-esp32-elf-g++".to_string())
        .load_device_config(manifest_dir.clone().join("device.toml"));
    
    match libs {
        Ok(lines) => {
            
            let b = b
                .flag(OsString::from("-mlongcalls"))
                .flag(OsString::from("-o"))
                .flag(OsString::from("firmware.elf"))
                // .flag(OsString::from("-lstdc++"))
                ;
                let mut lib_list = OsString::new();
                
                for line in lines {
                
                    if let Ok(lib) = line {
                    let orig = &PathBuf::from(&lib);
                    let filename = orig.file_name().unwrap();
                    let to = &PathBuf::from(out_dir).join(filename.to_str().unwrap());
                    let include = format!("-I{}", include_base.clone().join(orig.parent().unwrap().file_name().unwrap()).join("include").to_str().unwrap());
                    includes = format!("{} {}", includes, include);  
                    
                    
                    // Command::new(ar.as_str()).args(&["-x", filename.to_str().unwrap()])
                    // .current_dir(&Path::new(&out_dir.clone()))
                    // .status().unwrap();
                    println!("cargo:warning=copying lib from {:?} to {:?}", orig, to);
                    let libname = filename.to_str().unwrap()
                        .strip_prefix("lib")
                        .unwrap().strip_suffix(".a").unwrap();
                    // let b = b.flag(OsString::from(format!("-l{}", libname)));
                    lib_list.push(" ");
                    lib_list.push(to);
                    copy(orig, to).unwrap();
                }
            }

            // add wifi libs
            let mut wifi_deps = OsString::new();
            let wifi_lib_path = PathBuf::from(include_base.clone()).join("esp_wifi").join("lib").join("esp32");
            match read_dir(wifi_lib_path) {
                Err(err)  => println!("cargo:warning=Error reading wifi libs {:?}", err),
                Ok(rd) => {
                    for entry in rd {
                        match entry {
                            Err(err) => println!("cargo:warning=Error reading wifi lib entry {:?}", err),
                            Ok(e) => {
                                wifi_deps.push(" ");
                                wifi_deps.push(e.path());
                            }
                        }
                    }
                }
            }
            
            let b = b
                .flag(lib_list.clone())
                .flag(OsString::from("-Wl,--cref"))
                .flag(OsString::from("-fno-rtti"))
                .flag(OsString::from("-fno-lto"))
                .flag(lib_list.clone())
                .flag(wifi_deps.clone())
                .flag(OsString::from("-u"))
                .flag(OsString::from("__cxa_guard_dummy"))
                .flag(OsString::from("-lstdc++"))
                .flag(OsString::from(PathBuf::from(out_dir).join("libpthread.a")))
                .flag(OsString::from("-u"))
                .flag(OsString::from("__cxx_fatal_exception"))
                .flag(OsString::from("-lm"))
                .flag(OsString::from(PathBuf::from(out_dir).join("libnewlib.a")))
                .flag(OsString::from("-u"))
                .flag(OsString::from("newlib_include_locks_impl"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("newlib_include_heap_impl"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("newlib_include_syscalls_impl"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("newlib_include_pthread_impl"))
                .flag(OsString::from("-lgcov"))
                .flag(OsString::from(out_dir.clone().join("libapp_trace.a")))
                .flag(OsString::from("-lgcov"))
                .flag(OsString::from("-lgcc"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("call_user_start_cpu0"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("ld_include_panic_highint_hdl"))
                .flag(OsString::from(include_base.clone().join("xtensa").join("esp32").join("libhal.a")))
                .flag(OsString::from("-u"))
                .flag(OsString::from("esp_app_desc"))
                .flag(OsString::from(out_dir.clone().join("libapp_trace.a")))
                .flag(OsString::from("-lc"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("pthread_include_pthread_impl"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("pthread_include_pthread_cond_impl"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("pthread_include_pthread_local_storage_impl"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("vfs_include_syscalls_impl"))
                .flag(OsString::from("-u"))
                .flag(OsString::from("vRingbufferReturnItem"))
                .flag(OsString::from(out_dir.clone().join("libesp_ringbuf.a")))
                .flag(OsString::from("-u"))
                .flag(OsString::from("esp_ota_get_running_partition"))
                .flag(OsString::from(out_dir.clone().join("libapp_update.a")))
                .flag(OsString::from("-L"))
                .flag(OsString::from(include_base.clone().join("esp_wifi").join("lib").join("esp32")))
            ;
            
            let out_include = &out_dir.join("include");
            includes = format!("{} -I{}", includes, out_include.display());
            let config = &out_include.clone().join("sdkconfig.h"); 
            create_dir(out_include).ok();
            
            copy(manifest_dir
                .join("lib")
                .join("build")
                .join("config")
                .join("sdkconfig.h")
                , config
            ).unwrap();

            let cmd = b.prepare_linker().link_cmd();
            let mut cmd_str = String::from("");
            for arg in cmd.iter() {
                cmd_str = format!("{} {}", cmd_str, arg.to_str().unwrap())
            }
            
            let cmd_file = out_dir.join("link.txt");
            if let Err(err) = write(cmd_file.clone(), cmd_str.to_owned().to_string()) {
                println!("cargo:warning={:?}", err);
            }

            println!("cargo:LINK_CMD={}", cmd_file.to_str().unwrap());
            
            // if let Err(_) = remove_file(out_dir.join("libidfsys.a")) {
            //     // ignore for now
            // }
            
            // // list all obj files
            // let dir = read_dir(out_dir.clone()).unwrap();
            // let obj = dir
            // .filter_map(|e| e.ok())
            // .filter(|e| e.file_name().to_str().unwrap().ends_with(".obj"))
            //     .map(|e| e.path().to_str().unwrap().to_string());
                
            // // run ar command to create one library
            // let mut args = svec!["-q", "-c", "-v", "libidfsys.a"];
            // args.extend(obj);
            // let run = Command::new(ar.as_str()).args(&args)
            // .current_dir(&Path::new(&out_dir.clone()))
            // .output();
            
            // match run {
            //     Err(err)=> println!("cargo:warning=error creating idfsys lib {:?}", err),
            //     Ok(out) => {
            //         let stdout = out.stdout.iter().map(|&c| c as char).collect::<String>();
            //         let stderr = out.stderr.iter().map(|&c| c as char).collect::<String>();
            //         println!("cargo:warning=command ended with");
            //         println!("cargo:warning=----------stdout:");
            //         println!("cargo:warning={}", stdout);
            //         println!("cargo:warning=----------stderr:");
            //         println!("cargo:warning={}", stderr);
            //     }
            // } 
            
            // println!("cargo:rustc-link-search=native={}", out_dir.clone().to_str().unwrap());
            // println!("cargo:rustc-link-lib=static=idfsys");
            println!("cargo:WRAPPER_INCLUDE={}", includes);
            // println!("cargo:rustc-env=WRAPPER_INCLUDE={}", inc);
        },
        Err(e) => {
            println!("cargo:warning=Cannot determine libraries: {:?}", e);
            return;
        }
    }
    println!("cargo:rerun-if-changed=lib/sdkconfig");
    println!("cargo:rerun-if-changed=device.toml");
}