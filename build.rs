
// extern crate bindgen;

use std::env;
use std::ffi::{OsString, OsStr};
use std::path::{Path, PathBuf};
use std::fs::File;
use std::io::Write;
use std::process::Command;

// macro_rules! svec {
//     ($($x:expr),*) => (vec![$($x.to_string()),*]);
// }

fn main() {
    let sys_dir = PathBuf::from(env::var("CARGO_MANIFEST_DIR").unwrap());
    let inc = env::var("DEP_IDFSYS_WRAPPER_INCLUDE").unwrap_or_default();
    let ld  = env::var("DEP_IDFSYS_LINK_CMD").unwrap_or_default();
    println!("cargo:LINK_CMD={}", ld);
    
    println!("cargo:env=LD_LIBRARY_PATH={}", "/rust-xtensa/build/x86_64-unknown-linux-gnu/llvm/lib/");
    println!("cargo:warning=running bindgen with wrapper {}", sys_dir.clone().join("generate/freertos_wrapper.h").to_str().unwrap());
    let target = env::var("TARGET").unwrap();
    println!("cargo:env=TARGET={}", "x86_64-unknown-linux-gnu");
    let mut clang_args = Vec::new();
    clang_args.push("-m32".to_string());
    // clang_args.push("-target".to_string());
    // clang_args.push(env::var("TARGET").unwrap());
    clang_args.push("--sysroot=/esp/xtensa-esp32-elf/xtensa-esp32-elf".to_string());
    clang_args.push("-I/home/rust/esp-idf/components/xtensa/esp32/include".to_string());
    clang_args.push("-I/home/rust/esp-idf/components/esp_rom/include".to_string());
    clang_args.push("-I/home/rust/esp-idf/components/soc/esp32/include".to_string());
    
    match shlex::split(&inc) {
        Some(strings) => {
            clang_args.extend(strings);
        }
        None => {
            clang_args.push(inc);
        }
    }
    
    // // workaround to turn glibc off. this should be removed once we can use a proper clang build
    // clang_args.push("-D__GLIBC_USE\\(...\\)=0".to_string());
    
    println!("cargo:warning=running bindgen with clang args {:?}", clang_args);
    // let result = bindgen::Builder::default()
    // .clang_args(clang_args)
    // .generate_comments(true)
    // .detect_include_paths(true)
    // .rust_target(bindgen::RustTarget::Nightly)
    // // The input header we would like to generate
    // // bindings for.
    // .header(sys_dir.clone().join("generate").join("freertos_wrapper.h").to_str().unwrap())
    // // Tell cargo to invalidate the built crate whenever any of the
    // // included header files changed.
    // .parse_callbacks(Box::new(bindgen::CargoCallbacks))
    // // Finish the builder and generate the bindings.
    // .generate();
    // 
    // match result {
        //     Err(_) => (),
        //     Ok(bindings) => {
            //         // Write the bindings to the $sys_dir/bindings.rs file.
            //         bindings
            //             .write_to_file(sys_dir.join("src/binary/freertos.rs"))
            //             .expect("Couldn't write bindings!");    
            //     }
            // }

    let path = env::var("PATH").or_else::<String, _>(|_| Ok("/bin:/usr/bin:/home/rust/.cargo/bin".to_string())).unwrap();            
            
    // let run = Command::new("./generate.sh").args(&clang_args)
    //     .current_dir(sys_dir)
    //     .env("TARGET", "x86_64-unknown-linux-gnu")
    //     .env("LD_LIBRARY_PATH", "/rust-xtensa/build/x86_64-unknown-linux-gnu/llvm/lib/")
    //     .env("PATH", path)
    //     .output();
    
    // match run {
    //     Err(err)=> println!("cargo:warning=error creating idfsys crate {:?}", err),
    //     Ok(out) => {
    //         let stdout = out.stdout.iter().map(|&c| c as char).collect::<String>();
    //         let stderr = out.stderr.iter().map(|&c| c as char).collect::<String>();
    //         println!("cargo:warning=bindgen command ended with");
    //         println!("cargo:warning=----------stdout:");
    //         println!("cargo:warning={} ", stdout);
    //         println!("cargo:warning=----------stderr:");
    //         println!("cargo:warning={} ", stderr);
    //     }
    // } 
    
    println!("cargo:env=TARGET={}", target);
    
    println!("cargo:rerun-if-changed=generate.sh");
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=generate/esp32_wrapper.h");
    println!("cargo:rerun-if-changed=generate/freertos_wrapper.h");
}