//! Compatibility module for the ESP IDF framework
//! 
extern crate alloc;
use alloc::boxed::Box;
use crate::binary::esp32::va_list;

/// Converts a C pointer to a rust struct
pub trait FromIdf<IT>: Sized {

    /// Converts a C struct reference to a rust struct
    unsafe fn from_ptr(struct_ptr: *mut IT) -> Self {
        let b = Box::from_raw(struct_ptr);
        Self::map(*b)
    }

    fn map(cstruct: IT) -> Self;
}

type esp_log_level_t = cty::c_uint;

#[no_mangle]
pub extern "C" fn esp_log_writev(
    level: esp_log_level_t,
    tag: *const cty::c_char,
    format: *const cty::c_char,
    args: va_list,
) {
    
}

/// Peripheral wrapper
///
///
pub mod hal {

    use crate::prelude::*;
    use core::fmt::Display;
    use super::alloc::string::String;
    
    pub use esp32_hal::dport::Split;
    use core::default::Default;
    use core::ops::Deref;
    use core::mem;

    
    pub trait Named {
        fn name(&self) -> String;
    }

    pub trait BlockRef: Deref{
        type Output;

        fn block_ref(&self) -> Self::Output;
    }

    pub trait Splittable: Named {
        type Res;

        fn split(&self) -> Self::Res;
    }

    // #[derive(Debug)]
    pub enum Peripheral<'peripherals, R> {
        Splittable(&'peripherals dyn Splittable<Res=R>),
        Pins(&'peripherals R),
        None,
    }

    impl<'peripherals, R> Default for Peripheral<'peripherals, R> {
        fn default() -> Self {
            Self::None
        }
    }    

    impl<'peripherals, R> Peripheral<'peripherals, R> {

        /// Returns `true` if the peripheral is *not* a [`None`] value.
        ///
        /// # Examples
        ///
        /// ```
        /// type Pins = (u8, u8);
        /// let x: Peripheral<Pins> = Peripheral((0, 0));
        /// assert_eq!(x.is_present(), true);
        ///
        /// let x: Peripheral<Pins> = None;
        /// assert_eq!(x.is_present(), false);
        /// ```
        #[inline]
        pub const fn is_present(&self) -> bool {
            !matches!(*self, Self::None)
        }

        /// Returns `true` if the option is a [`None`] value.
        ///
        /// # Examples
        ///
        /// ```
        /// type Pins = (u8, u8);
        /// let x: Peripheral<Pins> = Peripheral((0, 0));
        /// assert_eq!(x.is_none(), true);
        ///
        /// let x: Peripheral<Pins> = None;
        /// assert_eq!(x.is_none(), false);
        /// ```
        #[inline]
        pub const fn is_none(&self) -> bool {
            !self.is_present()
        }



        /// Takes the value out of the peripheral, leaving a [`None`] in its place.
        ///
        /// # Examples
        ///
        /// ```
        /// type Pins = (u8, u8);
        /// let mut x = Peripheral::Pins((0, 0));
        /// let y = x.take();
        /// assert_eq!(x, None);
        /// assert_eq!(y, Peripheral::Pins((0, 0)));
        ///
        /// let mut x: Peripheral<Pins> = None;
        /// let y = x.take();
        /// assert_eq!(x, None);
        /// assert_eq!(y, None);
        /// ```
        #[inline]
        pub fn take(&mut self) -> Peripheral<'peripherals, R> {
            mem::take(self)
        }

        /// Returns the contained [`Splittable`] value, consuming the `self` value.
        ///
        /// # Panics
        ///
        /// Panics if the value is not a [`Splittable`] with a custom panic message provided by
        /// `msg`.
        ///
        /// # Examples
        ///
        /// ```
        /// struct SplitResult(u8, u8);
        /// struct UART0 {}
        /// impl Splittable for UART0 {
        ///     type Res = SplitResult;    
        ///     fn split(&self) -> Self::Res {
        ///         SplitResult(0,0)
        //      }
        /// }
        /// let splittable = UART0{};
        /// let x = Peripheral::Splittable(&splittable);
        /// assert_eq!(x.expect("fruits are healthy"), SplitResult(0,0));
        /// ```
        ///
        /// ```{.should_panic}
        /// let x: Peripheral<SplitResult> = Peripheral::None;
        /// x.expect("fruits are healthy"); // panics with `fruits are healthy`
        /// ```
        #[inline]
        pub fn expect_splittable(&mut self, msg: &str) -> &'peripherals dyn Splittable<Res=R> {
            match self {
                Peripheral::Splittable(val) => *val,
                _ => crate::common::expect_failed(msg),
            }
        }

        /// Returns the contained [`Pins`] value, consuming the `self` value.
        ///
        /// # Panics
        ///
        /// Panics if the value is not a [`Pins`] with a custom panic message provided by
        /// `msg`.
        ///
        /// # Examples
        ///
        /// ```
        /// struct Pins(u8, u8);
        /// let x = Peripheral::Pins(Pins(0, 0));
        /// assert_eq!(x.expect("fruits are healthy"), Pins(0, 0));
        /// ```
        ///
        /// ```{.should_panic}
        /// let x: Peripheral<Pins> = Peripheral::None;
        /// x.expect("fruits are healthy"); // panics with `fruits are healthy`
        /// ```
        #[inline]
        pub fn expect_pins(&mut self, msg: &str) -> &'peripherals R {
            match self {
                Peripheral::Pins(val) => val,
                _ => crate::common::expect_failed(msg),
            }
        }

    }

}