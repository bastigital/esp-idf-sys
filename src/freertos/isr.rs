// #![cfg(feature = "isr")]

use crate::freertos::base::*;
use crate::freertos::ffi::*;

/// Keep track of whether we need to yield the execution to a different
/// task at the end of the interrupt.
///
/// Should be dropped as the last thing inside a interrupt.
pub struct InterruptContext {
    x_higher_priority_task_woken: BaseType,
}

impl InterruptContext {
    /// Instantiate a new context.
    pub fn new() -> InterruptContext {
        InterruptContext { x_higher_priority_task_woken: 0 }
    }

    pub unsafe fn get_task_field_mut(&self) -> BaseTypeMutPtr {
        self.x_higher_priority_task_woken as *mut _
    }
}

impl Drop for InterruptContext {
    fn drop(&mut self) {
        if self.x_higher_priority_task_woken == 1 {
            unsafe { isr_yield(); }
        }
    }
}
