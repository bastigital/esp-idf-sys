// #![cfg(feature = "critical")]

use crate::freertos::prelude::*;
use crate::freertos::base::*;
use crate::freertos::shim::*;

pub struct CriticalRegion {
    ptr: Option<*mut i32>,
}

impl CriticalRegion {
    pub fn enter() -> Self {
        let lock = CriticalRegion{ptr: None};
        let b  = Box::new(lock); 
        let ptr = Box::into_raw(b) as *mut i32;
        let lock = CriticalRegion{ptr: Some(ptr)};
        unsafe { enter_critical( ptr); }
        lock
    }
}

impl Drop for CriticalRegion {
    fn drop(&mut self) {
        let ptr= self.ptr.unwrap();
        unsafe { 
            let _ = Box::from_raw(ptr as *mut i32);
            exit_critical(ptr); 
        }
    }
}

unsafe impl<T: Sync + Send> Send for ExclusiveData<T> {}
unsafe impl<T: Sync + Send> Sync for ExclusiveData<T> {}

/// Data protected with a critical region. Lightweight version of a mutex,
/// intended for simple data structures.
pub struct ExclusiveData<T: ?Sized> {
    data: UnsafeCell<T>
}

impl<T> ExclusiveData<T> {
    pub fn new(data: T) -> Self {
        ExclusiveData {
            data: UnsafeCell::new(data)
        }
    }

    pub fn lock(&self) -> Result<ExclusiveDataGuard<T>, FreeRtosError> {
        Ok(ExclusiveDataGuard {
            __data: &self.data,
            __lock: CriticalRegion::enter()
        })
    }

    pub fn lock_from_isr(&self, _context: &mut crate::isr::InterruptContext) -> Result<ExclusiveDataGuardIsr<T>, FreeRtosError> {
        Ok(ExclusiveDataGuardIsr {
            __data: &self.data            
        })
    }
}

/// Holds the mutex until we are dropped
pub struct ExclusiveDataGuard<'a, T: ?Sized + 'a> {
    __data: &'a UnsafeCell<T>,
    __lock: CriticalRegion
}

impl<'mutex, T: ?Sized> Deref for ExclusiveDataGuard<'mutex, T> {
    type Target = T;

    fn deref<'a>(&'a self) -> &'a T {
        unsafe { &*self.__data.get() }
    }
}

impl<'mutex, T: ?Sized> DerefMut for ExclusiveDataGuard<'mutex, T> {
    fn deref_mut<'a>(&'a mut self) -> &'a mut T {
        unsafe { &mut *self.__data.get() }
    }
}


pub struct ExclusiveDataGuardIsr<'a, T: ?Sized + 'a> {
    __data: &'a UnsafeCell<T>
}

impl<'mutex, T: ?Sized> Deref for ExclusiveDataGuardIsr<'mutex, T> {
    type Target = T;

    fn deref<'a>(&'a self) -> &'a T {
        unsafe { &*self.__data.get() }
    }
}

impl<'mutex, T: ?Sized> DerefMut for ExclusiveDataGuardIsr<'mutex, T> {
    fn deref_mut<'a>(&'a mut self) -> &'a mut T {
        unsafe { &mut *self.__data.get() }
    }
}
