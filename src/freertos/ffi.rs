#![allow(non_snake_case)]
///!
///!
///!
///!
///!
///!
///!
///!
///!
///!
///!
///!
///!
///!
use core::convert::TryInto;

use crate::freertos::prelude::*;
use crate::binary::freertos as bin;
use crate::freertos::base::*;
use crate::freertos::utils::*;

//
// Constants copied from freertos for 
//
pub const portTICK_PERIOD_MS: TickType = 1000 / bin::configTICK_RATE_HZ;
pub const queueSEND_TO_BACK: BaseType = 0;
pub const queueSEND_TO_FRONT: BaseType = 1;
pub const queueOVERWRITE: BaseType = 2;

// For internal use only.  These definitions *must* match those in freertos / queue.c
pub const queueQUEUE_TYPE_BASE: cty::uint8_t = 0;
pub const queueQUEUE_TYPE_SET: cty::uint8_t = 0;
pub const queueQUEUE_TYPE_MUTEX: cty::uint8_t = 1;
pub const queueQUEUE_TYPE_COUNTING_SEMAPHORE: cty::uint8_t = 2;
pub const queueQUEUE_TYPE_BINARY_SEMAPHORE: cty::uint8_t = 3;
pub const queueQUEUE_TYPE_RECURSIVE_MUTEX: cty::uint8_t = 4;

pub const semBINARY_SEMAPHORE_QUEUE_LENGTH: cty::uint8_t = 1;
pub const semSEMAPHORE_QUEUE_ITEM_LENGTH: cty::uint8_t = 0;
pub const semGIVE_BLOCK_TIME: TickType = 0;

pub const portMAX_DELAY: TickType = 0xffffffff;

pub const tmrCOMMAND_EXECUTE_CALLBACK_FROM_ISR: BaseType = -2;
pub const tmrCOMMAND_EXECUTE_CALLBACK: BaseType = -1;
pub const tmrCOMMAND_START_DONT_TRACE: BaseType = 0;
pub const tmrCOMMAND_START: BaseType = 1;
pub const tmrCOMMAND_RESET: BaseType = 2;
pub const tmrCOMMAND_STOP: BaseType = 3;
pub const tmrCOMMAND_CHANGE_PERIOD: BaseType = 4;
pub const tmrCOMMAND_DELETE: BaseType = 5;

pub const tmrFIRST_FROM_ISR_COMMAND: BaseType = 6;
pub const tmrCOMMAND_START_FROM_ISR: BaseType = 6;
pub const tmrCOMMAND_RESET_FROM_ISR: BaseType = 7;
pub const tmrCOMMAND_STOP_FROM_ISR: BaseType = 8;
pub const tmrCOMMAND_CHANGE_PERIOD_FROM_ISR: BaseType = 9;

pub const pdTRUE: BaseType = 1;
pub const pdFALSE: BaseType = 0;
pub const pdPASS: BaseType = pdTRUE;

pub fn v_task_start_scheduler() -> ! {
	unsafe { bin::vTaskStartScheduler() };
	loop{
		unreachable!()
	}
}

pub fn pv_port_malloc(x_wanted_size: UBaseType) -> MutVoidPtr {
    unsafe { bin::malloc(x_wanted_size) as MutVoidPtr }
}

pub fn v_port_free(pv: MutVoidPtr) {
    unsafe { bin::free(pv) };
}

pub fn sizeof(_type: cty::uint8_t) -> cty::uint8_t {
	match _type {
		0 => mem::size_of::<VoidPtr>().try_into().unwrap(),
		1 => mem::size_of::<CharPtr>().try_into().unwrap(),
		2 => mem::size_of::<char>().try_into().unwrap(),

		10 => mem::size_of::<BaseType>().try_into().unwrap(),
		11 => mem::size_of::<UBaseType>().try_into().unwrap(),
		12 => mem::size_of::<TickType>().try_into().unwrap(),

		20 => mem::size_of::<TaskHandle>().try_into().unwrap(),
		21 => mem::size_of::<QueueHandle>().try_into().unwrap(),
		22 => mem::size_of::<SemaphoreHandle>().try_into().unwrap(),
		23 => mem::size_of::<TaskFunction>().try_into().unwrap(),
		24 => mem::size_of::<TimerHandle>().try_into().unwrap(),
		25 => mem::size_of::<TimerCallback>().try_into().unwrap(),

		30 => mem::size_of::<TaskStatus>().try_into().unwrap(),
		31 => mem::size_of::<TaskState>().try_into().unwrap(),
		32 => mem::size_of::<UnsignedLong>().try_into().unwrap(),
		33 => mem::size_of::<UnsignedShort>().try_into().unwrap(),
        _ => 0,
	}
}

pub fn v_task_delay_until(px_previous_wake_time: *mut TickType, x_time_increment: TickType ) {
    if bin::INCLUDE_vTaskDelayUntil == 1 {
        unsafe { bin::vTaskDelayUntil(px_previous_wake_time, x_time_increment) };
    }
}

pub fn v_task_delay(x_ticks_to_delay: TickType) {
    if bin::INCLUDE_vTaskDelay == 1 {
        unsafe { bin::vTaskDelay(x_ticks_to_delay) };
    }
}

pub fn x_task_get_tick_count() -> TickType {
    unsafe { bin::xTaskGetTickCount() }
}

pub fn get_system_state(px_task_status_array: *mut TaskStatus, 
						ux_array_size: UBaseType, 
						pul_total_run_time: *mut cty::c_uint) -> UBaseType {
	unsafe { bin::uxTaskGetSystemState(px_task_status_array, ux_array_size, pul_total_run_time) }
}

pub fn get_port_tick_period_ms() -> TickType {
	portTICK_PERIOD_MS
}

pub fn get_number_of_tasks() -> UBaseType {
	unsafe { bin::uxTaskGetNumberOfTasks() }
}

pub fn create_recursive_mutex() -> QueueHandle {
    if bin::configUSE_RECURSIVE_MUTEXES != 1 || bin::configSUPPORT_DYNAMIC_ALLOCATION != 1 {
        panic!("attempting to call xSemaphoreCreateRecursiveMutex, but configUSE_RECURSIVE_MUTEXES is not 1");
    }
	unsafe { bin::xQueueCreateMutex( queueQUEUE_TYPE_RECURSIVE_MUTEX ) }
}

pub fn take_recursive_mutex(mutex: MutQueueHandle, max: UBaseType) -> UBaseType {
    if bin::configUSE_RECURSIVE_MUTEXES != 1 || bin::configSUPPORT_DYNAMIC_ALLOCATION != 1 {
        panic!("attempting to call xQueueTakeMutexRecursive, but configUSE_RECURSIVE_MUTEXES is not 1");
    }

	if unsafe { bin::xQueueTakeMutexRecursive( mutex as MutQueueHandle, max ) } == pdTRUE {
        return 0;
    }
    return 1;
}

pub fn give_recursive_mutex(mutex: MutQueueHandle) -> UBaseType {
    if bin::configUSE_RECURSIVE_MUTEXES != 1 || bin::configSUPPORT_DYNAMIC_ALLOCATION != 1 {
        panic!("attempting to call xQueueGiveMutexRecursive, but configUSE_RECURSIVE_MUTEXES is not 1");
    }

	if unsafe { bin::xQueueGiveMutexRecursive( mutex as MutQueueHandle) } == pdTRUE {
        return 0;
    }
    return 1;
}

pub fn create_mutex() -> QueueHandle {
	unsafe { bin::xQueueCreateMutex(queueQUEUE_TYPE_MUTEX) }
}

pub fn create_binary_semaphore() -> QueueHandle {
	unsafe { bin::xQueueCreateMutex(queueQUEUE_TYPE_BINARY_SEMAPHORE) }
}

pub fn create_counting_semaphore(max: UBaseType, initial: UBaseType) -> QueueHandle {
	unsafe { bin::xQueueCreateCountingSemaphore(max, initial) }
}

pub fn delete_semaphore(semaphore: MutQueueHandle) {
	unsafe { bin::vQueueDelete(semaphore) };
}

fn xSemaphoreTake(mutex: MutQueueHandle, max: UBaseType) -> BaseType {
    unsafe { bin::xQueueGenericReceive( mutex, core::ptr::null::<cty::c_void>() as MutVoidPtr, max, pdFALSE ) }
}

pub fn take_mutex(mutex: MutQueueHandle, max: UBaseType) -> BaseType {
	if xSemaphoreTake(mutex, max) == pdTRUE {
		return 0;
	}
	return 1;
}

fn xSemaphoreGive(mutex: MutQueueHandle) -> BaseType {
    unsafe { bin::xQueueGenericSend( mutex, core::ptr::null::<cty::c_void>() as MutVoidPtr, semGIVE_BLOCK_TIME, queueSEND_TO_BACK ) }
}

pub fn give_mutex(mutex: MutQueueHandle) -> BaseType {
	if xSemaphoreGive(mutex) == pdTRUE {
		return 0;
	}
	return 1;
}

fn xSemaphoreTakeFromISR(semaphore: MutQueueHandle, px_higher_priority_task_woken: BaseTypeMutPtr) -> BaseType {
    unsafe { bin::xQueueReceiveFromISR( semaphore, core::ptr::null::<cty::c_void>() as MutVoidPtr, px_higher_priority_task_woken ) }
}

pub fn take_semaphore_isr(semaphore: MutQueueHandle, px_higher_priority_task_woken: BaseTypeMutPtr) -> BaseType {
	if xSemaphoreTakeFromISR(semaphore, px_higher_priority_task_woken) == pdTRUE {
		return 0;
	}

	return 1;
}

fn xSemaphoreGiveFromISR(semaphore: MutQueueHandle, px_higher_priority_task_woken: BaseTypeMutPtr) -> BaseType {
    unsafe { bin::xQueueGiveFromISR( semaphore, px_higher_priority_task_woken ) }
}

pub fn give_semaphore_isr(semaphore: MutQueueHandle, px_higher_priority_task_woken: BaseTypeMutPtr) -> UBaseType {
	if xSemaphoreGiveFromISR(semaphore, px_higher_priority_task_woken) == pdTRUE {
		return 0;
	}

	return 1;
}

pub fn spawn_task(entry_point: Option<unsafe extern fn(MutVoidPtr)>, 
					pv_parameters: MutVoidPtr, 
					name: *const cty::c_char, 
					name_len: cty::uint8_t,  
					stack_size: UBaseType, 
					priority: UBaseType, 
					task_handle: *mut TaskHandle,
					core_id: BaseType) 
					-> UBaseType {
	let s_name = unsafe { str_from_c_string(name).unwrap() };
	const max_len: usize = bin::configMAX_TASK_NAME_LEN as usize;
	let s_name = if s_name.len() > max_len { &s_name[..max_len] } else { &s_name[..] };

	let ret = unsafe { 
		
		bin::xTaskCreatePinnedToCore(entry_point, 
										s_name.as_ptr() as *const i8, 
										stack_size, 
										pv_parameters, 
										priority, 
										task_handle, 
										core_id)
	};

	if ret != pdPASS {
		return 1;
	}

	return 0;
}

pub fn delete_task(task: TaskHandle) {
    if bin::INCLUDE_vTaskDelete != 1 {
        panic!("attempting to call vTaskDelete, but INCLUDE_vTaskDelete is not 1");
    }
	unsafe { bin::vTaskDelete(task) }
}

pub fn get_stack_high_water_mark(task: TaskHandle) -> UBaseType {
    if bin::INCLUDE_uxTaskGetStackHighWaterMark == 1 {
        return unsafe { bin::uxTaskGetStackHighWaterMark(task) };
    } else {
        return 0;
    }
}

pub fn queue_create(queue_length: UBaseType, item_size: UBaseType) -> QueueHandle {

	unsafe { 
		
		bin::xQueueCreateMutex(queueQUEUE_TYPE_SET) 
								
	}

}

pub fn queue_delete(queue: MutQueueHandle) {
	unsafe { bin::vQueueDelete(queue) };
}

fn xQueueSendToBack(queue: MutQueueHandle, item: VoidPtr, max_wait: TickType) -> BaseType {
    unsafe { bin::xQueueGenericSend(queue, item, max_wait, queueSEND_TO_BACK ) }
}

pub fn queue_send(queue: MutQueueHandle, item: VoidPtr, max_wait: TickType) -> UBaseType {
    if unsafe { xQueueSendToBack(queue, item, max_wait ) } != pdTRUE {
        return 1;
	}
	return 0;
}

fn xQueueSendToBackFromISR(queue: MutQueueHandle, item: VoidPtr, px_higher_priority_task_woken: *mut BaseType) -> BaseType {
	
	unsafe {
		bin::xQueueGenericSendFromISR(queue
								, item
								, px_higher_priority_task_woken
								, queueSEND_TO_BACK 
							) 
	}
}

pub fn queue_send_isr(queue: MutQueueHandle, item: VoidPtr, px_higher_priority_task_woken: *mut BaseType) -> UBaseType {
	if unsafe { xQueueSendToBackFromISR(queue, item, px_higher_priority_task_woken) } == pdTRUE {
		return 0;
	}
	return 1;
}

pub fn queue_receive(queue: MutQueueHandle, item: MutVoidPtr, max_wait: TickType) -> UBaseType {
	if unsafe { bin::xQueueGenericReceive( queue, item, max_wait, pdFALSE) } != pdTRUE {
		return 1;
	}
	return 0;
}

pub fn isr_yield() {
    unsafe { bin::vPortYield() };
}

pub fn max_wait() -> TickType {
	portMAX_DELAY
}

pub fn task_get_name(task: TaskHandle) -> *mut cty::c_char {
	unsafe { bin::pcTaskGetTaskName(task) }
}

pub fn task_notify_take(clear_count: cty::uint8_t, wait: TickType) -> UBaseType {
    let clear_count_on_exit = if clear_count == 1 { pdTRUE } else { pdFALSE };
	unsafe { bin::ulTaskNotifyTake(clear_count_on_exit, wait) }
}

pub fn task_notify_wait(ul_bits_to_clear_on_entry: cty::uint32_t
						, ul_bits_to_clear_on_exit: cty::uint32_t
						, pul_notification_value: *mut cty::uint32_t
						, x_ticks_to_wait: TickType
					) -> BaseType {

	if unsafe { bin::xTaskNotifyWait(ul_bits_to_clear_on_entry
									, ul_bits_to_clear_on_exit
									, pul_notification_value
									, x_ticks_to_wait
								) } == pdTRUE {
		return 0;
	}
	return 1;
}

pub fn task_notify_action(action: cty::uint8_t) -> bin::eNotifyAction {
	match action {
		1 => bin::eNotifyAction_eSetBits,
		2 => bin::eNotifyAction_eIncrement,
		3 => bin::eNotifyAction_eSetValueWithOverwrite,
		4 => bin::eNotifyAction_eSetValueWithoutOverwrite,
		_ => bin::eNotifyAction_eNoAction,
	}
}

pub fn task_notify(task: TaskHandle, value: cty::uint32_t, action: cty::uint8_t) -> BaseType {
	let e_action = task_notify_action(action);

	let v = unsafe { bin::xTaskNotify(task, value, e_action) };
	if v != pdPASS {
		return 1;
	}
	return 0;
}

pub fn task_notify_isr(task: TaskHandle, value: cty::uint32_t, action: cty::uint8_t, x_higher_priority_task_woken: BaseTypeMutPtr) -> BaseType {
	let e_action = task_notify_action(action);

	let v = unsafe { bin::xTaskNotifyFromISR(task, value, e_action, x_higher_priority_task_woken) };
	if v != pdPASS {
		return 1;
	}
	return 0;
}

pub fn get_current_task() -> TaskHandle {

	if bin::INCLUDE_xTaskGetCurrentTaskHandle == 0 
		&& bin::configUSE_MUTEXES == 0 {

		panic!("attempting to call xTaskGetCurrentTaskHandle, but neither INCLUDE_xTaskGetCurrentTaskHandle or configUSE_MUTEXES are enabled");

	}

    unsafe { bin::xTaskGetCurrentTaskHandle() }
	
}

pub fn timer_create(name: CharPtr, 
					name_len: cty::uint8_t, 
					period: TickType,
					auto_reload: cty::uint8_t, 
					timer_id: MutVoidPtr, 
					callback: Option<unsafe extern fn(TimerHandle) -> ()>,
				) -> TimerHandle
{
	let s_name = unsafe { str_from_c_string(name as *const i8) }.unwrap();

	let timer_auto_reload = if auto_reload == 1 { 1u32 } else {	0u32 };

	unsafe { 
		bin::xTimerCreate(s_name.as_ptr() as *const i8
			, period
			, timer_auto_reload
			, timer_id
			, callback)
	}
}

fn xTimerStart(timer: TimerHandle, block_time: TickType) -> BaseType {
	
	unsafe { 
		bin::xTimerGenericCommand(timer
								, tmrCOMMAND_START
								, bin::xTaskGetTickCount()
								, core::ptr::null_mut::<i32>()
								, block_time
							) 
	}

}

pub fn timer_start(timer: TimerHandle, block_time: TickType) -> BaseType {
    if xTimerStart(timer, block_time) != pdPASS {
        return 1;
	}
	return 0;
}

fn xTimerStop(timer: TimerHandle, block_time: TickType) -> BaseType {
	
	unsafe { 
		bin::xTimerGenericCommand(timer
								, tmrCOMMAND_STOP
								, 0
								, core::ptr::null_mut::<i32>()
								, block_time
							)
	}

}

pub fn timer_stop(timer: TimerHandle, block_time: TickType) -> BaseType {
    if xTimerStop(timer, block_time) != pdPASS {
        return 1;
	}
	return 0;
}

fn xTimerDelete(timer: TimerHandle, block_time: TickType) -> BaseType {

	unsafe { 
		bin::xTimerGenericCommand(timer
								, tmrCOMMAND_DELETE
								, 0
								, core::ptr::null_mut::<i32>()
								, block_time
							)
	}

}

pub fn timer_delete(timer: TimerHandle, block_time: TickType) -> BaseType {
    if xTimerDelete(timer, block_time) != pdPASS {
        return 1;
	}
	return 0;
}

fn xTimerChangePeriod(timer: TimerHandle, block_time: TickType, new_period: TickType) -> BaseType {

	unsafe { 
		bin::xTimerGenericCommand(timer
							, tmrCOMMAND_CHANGE_PERIOD
							, new_period
							, core::ptr::null_mut::<i32>()
							, block_time
						)
	}

}

pub fn timer_change_period(timer: TimerHandle, block_time: TickType, new_period: TickType) -> BaseType {
	if xTimerChangePeriod(timer, block_time, new_period) != pdPASS {
		return 1;
	}
	return 0;
}

pub fn timer_get_id(timer: TimerHandle) -> VoidPtr {
	unsafe { bin::pvTimerGetTimerID(timer) }
}

pub fn enter_critical(lock: MuxType) {
	unsafe { bin::vTaskEnterCritical(lock) };
}

pub fn exit_critical(lock: MuxType) {
	unsafe { bin::vTaskExitCritical(lock) };
}