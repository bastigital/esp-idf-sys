use crate::binary::freertos as bin;

/// Basic error type for the library.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum FreeRtosError {
    OutOfMemory,
    QueueSendTimeout,
    QueueReceiveTimeout,
    MutexTimeout,
    Timeout,
    QueueFull,
    StringConversionError,
    TaskNotFound,
    InvalidQueueSize,
    ProcessorHasShutDown,
}

pub type VoidPtr = *const cty::c_void;
pub type MutVoidPtr = *mut cty::c_void;
pub type CharPtr = *const cty::uint8_t;
pub type Char = cty::uint8_t;

pub type BaseType = cty::c_int;
pub type UBaseType = cty::c_uint;
pub type TickType = bin::TickType_t;
pub type BaseTypePtr = *const BaseType;
pub type BaseTypeMutPtr = *mut BaseType;

pub type TaskHandle = bin::TaskHandle_t;
pub type MutTaskHandle = *mut cty::c_void;
pub type QueueHandle = bin::QueueHandle_t;
pub type MutQueueHandle = *mut cty::c_void;
pub type SemaphoreHandle = bin::SemaphoreHandle_t;
pub type TaskFunction = *const cty::c_void;
pub type TimerHandle = bin::TimerHandle_t;
pub type TimerCallback = *const cty::c_void;
pub type MuxType = *mut bin::portMUX_TYPE;
#[allow(dead_code)]
pub type StackType = *const cty::c_void;

pub type UnsignedLong = cty::c_uint;
pub type UnsignedShort = cty::uint16_t;

pub type TaskStatus = crate::binary::freertos::xTASK_STATUS;

#[derive(Copy, Clone, Debug)]
#[repr(u8)]
pub enum TaskState {
    /// A task is querying the state of itself, so must be running.
    Running = 0,
    /// The task being queried is in a read or pending ready list.
    Ready = 1,
    /// The task being queried is in the Blocked state.
    Blocked = 2,
    /// The task being queried is in the Suspended state, or is in the Blocked state with an infinite time out.
    Suspended = 3,
    /// The task being queried has been deleted, but its TCB has not yet been freed.
    Deleted = 4,
}

impl From<u32> for TaskState {
    fn from(n: u32) -> Self {
        match n {
            0 => TaskState::Running,
            1 => TaskState::Ready,
            2 => TaskState::Blocked,
            3 => TaskState::Suspended,
            4 => TaskState::Deleted,
            _ => TaskState::Ready,
        }
    }
}
