// #![cfg(feature = "queue")]

use crate::freertos::base::*;
use crate::freertos::units::*;
use crate::freertos::ffi::*;

/// A counting or binary semaphore
pub struct Semaphore {
    semaphore: SemaphoreHandle,
}

unsafe impl Send for Semaphore {}
unsafe impl Sync for Semaphore {}

impl Semaphore {
    /// Create a new binary semaphore
    pub fn new_binary() -> Result<Semaphore, FreeRtosError> {
        unsafe {
            let s = create_binary_semaphore();
            if s == 0 as *mut _ {
                return Err(FreeRtosError::OutOfMemory);
            }
            Ok(Semaphore { semaphore: s })
        }
    }

    /// Create a new counting semaphore
    pub fn new_counting(max: u32, initial: u32) -> Result<Semaphore, FreeRtosError> {
        unsafe {
            let s = create_counting_semaphore(max, initial);
            if s == 0 as *mut _ {
                return Err(FreeRtosError::OutOfMemory);
            }
            Ok(Semaphore { semaphore: s })
        }
    }
    
    /// Lock this semaphore in a RAII fashion
    pub fn lock<D: DurationTicks>(&self, max_wait: D) -> Result<SemaphoreGuard, FreeRtosError> {
        unsafe {
            let res = take_mutex(self.semaphore as MutVoidPtr, max_wait.to_ticks());

            if res != 0 {
                return Err(FreeRtosError::Timeout);
            }

            Ok(SemaphoreGuard { __semaphore: self.semaphore })
        }
    }
}

impl Drop for Semaphore {
    fn drop(&mut self) {
        unsafe {
            delete_semaphore(self.semaphore as MutVoidPtr);
        }
    }
}

/// Holds the lock to the semaphore until we are dropped
pub struct SemaphoreGuard {
    __semaphore: SemaphoreHandle,
}

impl Drop for SemaphoreGuard {
    fn drop(&mut self) {
        unsafe {
            give_mutex(self.__semaphore as MutVoidPtr);
        }
    }
}
