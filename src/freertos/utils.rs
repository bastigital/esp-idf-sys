use crate::freertos::prelude::*;
use crate::freertos::ffi::*;
use crate::freertos::base::*;

#[derive(Debug, Copy, Clone)]
pub struct TypeSizeError {
    id: usize,
    c_size: usize,
    rust_size: usize
}

/// Perform checks whether the C FreeRTOS shim and Rust agree on the sizes of used types.
pub fn shim_sanity_check() -> Result<(), TypeSizeError> {

    let checks = [(0, mem::size_of::<VoidPtr>()),
                  (1, mem::size_of::<CharPtr>()),
                  (2, mem::size_of::<Char>()),

                  (10, mem::size_of::<BaseType>()),
                  (11, mem::size_of::<UBaseType>()),
                  (12, mem::size_of::<TickType>()),

                  (20, mem::size_of::<TaskHandle>()),
                  (21, mem::size_of::<QueueHandle>()),
                  (22, mem::size_of::<SemaphoreHandle>()),
                  (23, mem::size_of::<TaskFunction>()),
                  (24, mem::size_of::<TimerHandle>()),
                  (25, mem::size_of::<TimerCallback>()),

                  (30, mem::size_of::<TaskStatus>()),
                  (31, mem::size_of::<TaskState>()),
                  (32, mem::size_of::<UnsignedLong>()),
                  (33, mem::size_of::<UnsignedShort>())
                  ];

    for check in &checks {
        let c_size = unsafe { sizeof(check.0) };

        if c_size != check.1 as u8 {
            return Err(TypeSizeError {
                id: check.0 as usize,
                c_size: c_size as usize,
                rust_size: check.1
            });
        }
    }

    Ok(())
}

pub unsafe fn str_from_c_string(str: *const i8) -> Result<String, FreeRtosError> {
    let mut buf: Vec<u8> = Vec::new();

    let mut p = str;
    loop {
        if *p == 0 {
            break;
        }
        buf.push(*p as u8);
        p = p.offset(1);
    }

    match String::from_utf8(buf) {
        Ok(s) => Ok(s),
        Err(_) => Err(FreeRtosError::StringConversionError),
    }
}
