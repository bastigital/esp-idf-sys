#![no_std]
#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![feature(allocator_api)]
#![feature(lang_items, box_syntax)]
#![feature(nonnull_slice_from_raw_parts)]
#![feature(alloc_layout_extra)]
#![feature(const_fn)]
#![feature(trait_alias)]


extern crate cty;
extern crate esp32_hal;
extern crate esp32;
extern crate alloc as alloc_crate;

pub(crate) mod binary;
pub mod prelude;

pub use esp32_hal::{clock_control, dflush, dport, dprint, dprintln, get_core, get_other_core, target, analog, delay, efuse, mem, timer, units};

pub mod alloc;

pub mod compat;

pub mod log;

pub mod config;

pub mod platform;

pub mod serial;

pub mod freertos;

pub mod common {

    use crate::prelude::*;

    #[derive(Debug)]
    pub struct Error {
        /// More explanation of error that occurred.
        message: String,
    }
    
    impl Error {
        fn new(message: &str) -> Self {
            Error {
                message: String::from(message),
            }
        }
    }

    // This is a separate function to reduce the code size of .expect() itself.
    #[inline(never)]
    #[cold]
    #[track_caller]
    pub fn expect_failed(msg: &str) -> ! {
        panic!("{}", msg)
    }

}

pub mod util {

    
    use crate::binary::esp32::{rtc_cpu_freq_config_t, rtc_cpu_freq_src_t_RTC_CPU_FREQ_SRC_XTAL};

    impl Default for rtc_cpu_freq_config_t {
        fn default() -> Self {
            rtc_cpu_freq_config_t {
                source: rtc_cpu_freq_src_t_RTC_CPU_FREQ_SRC_XTAL,
                source_freq_mhz: 0,
                div: 0,
                freq_mhz: 0,
            }
        }
    }

}