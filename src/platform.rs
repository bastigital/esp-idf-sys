use crate::prelude::*;

// #[configure_peripherals[
    //     target::UART0
// ]]
// #[configure_peripherals[
//     aes(target::AES),
//     hinf(target::HINF),

//     spi(target::SPI),
//     spi(target::SPI1),
//     spi(target::SPI0),
//     spi(target::SPI2),
//     spi(target::SPI3),

//     i2c(target::I2C),
//     i2c(target::I2C0),
//     i2c(target::I2C1),

//     efuse(target::EFUSE),
//     pcnt(target::PCNT),
//     rmt(target::RMT),
//     gpio(target::GPIO),
    
//     uhci(target::UHCI),
//     uhci(target::UHCI0),
//     uhci(target::UHCI1),
    
//     uart(target::UART),
//     uart(target::UART0),
//     uart(target::UART1),
//     uart(target::UART2),

//     mcpwm(target::PWM0),
//     mcpwm(target::PWM1),
//     mcpwm(target::PWM2),
//     mcpwm(target::PWM3),
    
//     slchost(target::SLCHOST),
//     slc(target::SLC),
//     timg(target::TIMG1),
//     mcpwm(target::MCPWM),
    
//     rtccntl(target::RTCCNTL),
//     sens(target::SENS),
//     rtcio(target::RTCIO),
//     dport(target::DPORT),
//     timg(target::TIMG),
//     timg(target::TIMG0),
//     gpio_sd(target::GPIO_SD),
//     io_mux(target::IO_MUX),
//     i2s(target::I2S),
//     apb_ctrl(target::APB_CTRL),
//     syscon(target::SYSCON),
//     rtc_i2c(target::RTC_I2C),
//     ledc(target::LEDC),
// ]]
// #[configure_peripherals[
    // AES(target::AES),
    // HINF(target::HINF),

    // SPI(
    //     target::SPI1,
    //     target::SPI0,
    //     target::SPI2,
    //     target::SPI3,
    // ),

    // I2C(
    //     target::I2C0,
    //     target::I2C1,
    // ),

    // EFUSE(target::EFUSE),
    // PCNT(target::PCNT),
    // RMT(target::RMT),
    // GPIO(target::GPIO),
    
    // UHCI(
    //     target::UHCI0,
    //     target::UHCI1,
    // ),
    
    // UART(
    //     crate::serial::Instance,
    //     target::UART0,
    //     target::UART1,
    //     target::UART2,
    // ),

    // MCPWM(
    //     target::PWM0,
    //     target::PWM1,
    //     target::PWM2,
    //     target::PWM3,
    // ),
    
    // TIMG(
    //     target::TIMG0,
    //     target::TIMG1,
    // ),

    // SLCHOST(target::SLCHOST),
    // SLC(target::SLC),
    // RTCCNTL(target::RTCCNTL),
    // SENS(target::SENS),
    // RTCIO(target::RTCIO),
    // DPORT(target::DPORT),
    // GPIO_SD(target::GPIO_SD),
    // IO_MUX(target::IO_MUX),
    // I2S(target::I2S),
    // APB_CTRL(target::APB_CTRL),
    // SYSCON(target::SYSCON),
    // RTC_I2C(target::RTC_I2C),
    // LEDC(target::LEDC),
// ]]
pub mod peripherals {
    use crate::prelude::*;
    use crate::compat::hal::{BlockRef, Named};
    use crate::target;
    use esp32_hal::clock_control::{CPUSource, ClockControl, ClockControlConfig};
    use crate::compat::hal::Splittable;
    
    use esp32_hal::dport::Split;
    use crate::serial::Instance;

    // pub enum UART<'peripherals> {
    //     UART0(target::UART0),
    //     UART1(target::UART1),
    //     UART2(target::UART2),
    // }
    
    // pub struct Peripherals<'peripherals> {
    //     pub DPORT: Peripheral<'peripherals, dport::Result>,
    //     pub UART: Peripheral<'peripherals, serial::Result>,
    //     pub GPIO: Peripheral<'peripherals, gpio::Result>,
    //     dp: Box<&'peripherals target::Peripherals>,
    // }

    // impl<'peripherals> Peripherals<'peripherals> {

    //     pub fn setup(dp: &'peripherals target::Peripherals) -> Self {
    //         // let inner = &DPORT(dp.DPORT);
    //         // let dport_split: Peripheral<dport::Result> = Peripheral::Splittable(inner);
    //         // let split = dport_split.expect_splittable("DPORT is not splittable");

    //         // let dport_clock_control = split.split();

    //         // let mut clock_control = ClockControl::new(
    //         //     dp.RTCCNTL,
    //         //     dp.APB_CTRL,
    //         //     dport_clock_control,
    //         //     crate::clock_control::XTAL_FREQUENCY_AUTO,
    //         // )
    //         // .unwrap();

    //         // // set desired clock frequencies
    //         // clock_control
    //         //     .set_cpu_frequencies(
    //         //         CPUSource::Xtal,
    //         //         10.MHz(),
    //         //         CPUSource::PLL,
    //         //         240.MHz(),
    //         //         CPUSource::PLL,
    //         //         80.MHz(),
    //         //     )
    //         //     .unwrap();

    //         // let (mut clock_control_config, mut watchdog) = clock_control.freeze().unwrap();


    //         // watchdog.start(3.s());

    //         Peripherals {
    //             UART: Peripheral::default(),
    //             GPIO: Peripheral::default(),
    //             DPORT: Peripheral::default(),
    //             dp: Box::new(dp),
    //         }
    //     }
    // }

    // #[peripheral[target::DPORT]]
    pub mod dport {

        use esp32_hal::dport::*;
        use esp32_hal::dport::Split;
        use crate::{
            target,
            compat::hal::{Named, Splittable},
        };
        use core::ops::Deref;

        pub type Result = ClockControl;
        
        // impl Splittable for super::DPORT<target::DPORT> {
        //     type Res = Result;
            
        //     fn split(&self) -> Self::Res {
        //         let inner = unsafe { self.phantom };
        //         let s: (_, ClockControl) = inner.split();
        //         s.1
        //     }
        // }

    }
    
    // #[peripheral[target::GPIO]]
    pub mod gpio {
        use esp32_hal::gpio::*;
        use crate::{
            target,
            compat::hal::{ Named, Splittable },
        };

        pub struct Result {
            gpio0: Gpio0<Unknown>,
            gpio1: Gpio1<Unknown>,
            gpio2: Gpio2<Unknown>,
            gpio3: Gpio3<Unknown>,
            gpio4: Gpio4<Unknown>,
            gpio5: Gpio5<Unknown>,
            gpio6: Gpio6<Unknown>,
            gpio7: Gpio7<Unknown>,
            gpio8: Gpio8<Unknown>,
            gpio9: Gpio9<Unknown>,
            gpio10: Gpio10<Unknown>,
            gpio11: Gpio11<Unknown>,
            gpio12: Gpio12<Unknown>,
            gpio13: Gpio13<Unknown>,
            gpio14: Gpio14<Unknown>,
            gpio15: Gpio15<Unknown>,
            gpio16: Gpio16<Unknown>,
            gpio17: Gpio17<Unknown>,
            gpio18: Gpio18<Unknown>,
            gpio19: Gpio19<Unknown>,
            gpio20: Gpio20<Unknown>,
            gpio21: Gpio21<Unknown>,
            gpio22: Gpio22<Unknown>,
            gpio23: Gpio23<Unknown>,
            gpio25: Gpio25<Unknown>,
            gpio26: Gpio26<Unknown>,
            gpio27: Gpio27<Unknown>,
            gpio32: Gpio32<Unknown>,
            gpio33: Gpio33<Unknown>,
            gpio34: Gpio34<Unknown>,
            gpio35: Gpio35<Unknown>,
            gpio36: Gpio36<Unknown>,
            gpio37: Gpio37<Unknown>,
            gpio38: Gpio38<Unknown>,
            gpio39: Gpio39<Unknown>,
        }
        
        // impl Splittable for super::GPIO<target::GPIO> {
        //     type Res = Result;

        //     fn split(&self) -> Self::Res {
        //         self.name();
        //         let inner = unsafe { *self.phantom };
        //         let s = inner.split();
        //         Result {
        //             gpio0: s.gpio0,
        //             gpio1: s.gpio1,
        //             gpio2: s.gpio2,
        //             gpio3: s.gpio3,
        //             gpio4: s.gpio4,
        //             gpio5: s.gpio5,
        //             gpio6: s.gpio6,
        //             gpio7: s.gpio7,
        //             gpio8: s.gpio8,
        //             gpio9: s.gpio9,
        //             gpio10: s.gpio10,
        //             gpio11: s.gpio11,
        //             gpio12: s.gpio12,
        //             gpio13: s.gpio13,
        //             gpio14: s.gpio14,
        //             gpio15: s.gpio15,
        //             gpio16: s.gpio16,
        //             gpio17: s.gpio17,
        //             gpio18: s.gpio18,
        //             gpio19: s.gpio19,
        //             gpio20: s.gpio20,
        //             gpio21: s.gpio21,
        //             gpio22: s.gpio22,
        //             gpio23: s.gpio23,
        //             gpio25: s.gpio25,
        //             gpio26: s.gpio26,
        //             gpio27: s.gpio27,
        //             gpio32: s.gpio32,
        //             gpio33: s.gpio33,
        //             gpio34: s.gpio34,
        //             gpio35: s.gpio35,
        //             gpio36: s.gpio36,
        //             gpio37: s.gpio37,
        //             gpio38: s.gpio38,
        //             gpio39: s.gpio39,
        //         }
        //     }
        // }

        
        // impl core::iter::Enumerate for Enumerated {
        //     type Res = Enumerated;

        //     fn enumerate(self) -> Result<Self::Res, _> {
        //         match self {
        //             self.name(),
        //             self::Enumerated,
        //         }
        //     }
        // }

        // impl core::iter::IntoIterator<Enumerated> for GPIO {
        


        // }
    }

    pub mod serial {

        use crate::target;
        use crate::serial::*;
        use crate::compat::hal::Named;
        use esp32_hal::gpio::{GpioExt, Floating, Gpio1, Gpio3, Gpio19, Gpio22, Input, Output, PushPull, Unknown};
        use crate::compat::hal::Splittable;
        pub type Result = (Gpio1<Unknown>, Gpio3<Unknown>, Gpio19<Input<Floating>>, Gpio22<Output<PushPull>>);
        
        // impl Splittable for super::UART<target::UART> {
        //     type Res = Result;

        //     fn split(self) -> Self::Res {
        //         self.0.split()
        //     }
        // }
    }
}
