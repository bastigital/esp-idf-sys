

use crate::{prelude::*};
use core::fmt::Write;

#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub enum Level {
    TRACE = 0,
    DEBUG = 1,
    INFO = 2,
    WARN = 3,
    ERROR = 4,
}

impl Level {
    fn as_str(&self) -> &str {
        match self {
            Level::TRACE => "TRACE",
            Level::DEBUG => "DEBUG",
            Level::INFO => "INFO",
            Level::WARN => "WARN",
            Level::ERROR => "ERROR",
        }
    }
}

pub trait Logger: Write {

    fn trace(&mut self, tag: &str, msg: &str) {
        self.log(Level::TRACE, tag, msg);
    }
    
    fn debug(&mut self, tag: &str, msg: &str) {
        self.log(Level::DEBUG, tag, msg);
    }
    
    fn info(&mut self, tag: &str, msg: &str) {
        self.log(Level::INFO, tag, msg);
    }
    
    fn warn(&mut self, tag: &str, msg: &str) {
        self.log(Level::WARN, tag, msg);
    }
    
    fn error(&mut self, tag: &str, msg: &str) {
        self.log(Level::ERROR, tag, msg);
    }
    
    fn log(&mut self, level: Level, tag: &str, msg: &str);
}

// #[derive(Clone)]
pub struct Config<'tx, TX: Write> {
    pub level: Level,
    pub format: String,
    pub baudrate: Hertz,
    // peripherals: Option<Box<&'peripherals target::Peripherals>>,
    pub tx: &'tx mut TX,

}

#[derive(Debug, Clone)]
pub struct Error;


// static mut SERIAL: SerialLogger;

// #[configurable]
// pub struct SerialLogger<Config, Error> {
pub struct SerialLogger<'tx, TX: Write> {
    pub level: Level,
    tx: &'tx mut TX,
}

impl<'tx, TX: Write>  SerialLogger<'tx, TX> {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.tx.write_str(s)
    }
}

impl<'tx, TX: Write> core::convert::TryFrom<Config<'tx, TX>> for SerialLogger<'tx, TX> {
    type Error = Error;


    fn try_from(config: Config<'tx, TX>) -> Result<Self, Self::Error> {

        let log = {
            // setup serial controller
            let log = SerialLogger {
                level: config.level.clone(),
                tx: config.tx,
            };
            log
        };
        Ok(log)
    }
}

impl<'tx, TX: Write> Write for SerialLogger<'tx, TX> {
    fn write_str(&mut self, s: &str) -> core::fmt::Result {
        self.write_str(s)
    }
}

impl<'tx, TX: Write> Logger for SerialLogger<'tx, TX> {
    fn log(&mut self, level: Level, tag: &str, msg: &str) {
        if level >= self.level {
            let lv_str = level.as_str();
            self.write_str(format!("{}\t |{}\t| {}\n", lv_str, tag, msg).as_str()).unwrap();
        }
    }
}

#[macro_export]
macro_rules! logf {
    ($dst:expr, $level:path, $tag:tt, $($arg:tt)*) => (
        $dst.log($level, $tag, format!($($arg)*).as_str());
    );
}

#[macro_export]
macro_rules! tracef {
    ($dst:expr, $tag:tt, $($arg:tt)*) => (
        $dst.trace($tag, format!($($arg)*).as_str());
    );
}

#[macro_export]
macro_rules! debugf {
    ($dst:expr, $tag:tt, $($arg:tt)*) => (
        $dst.debug($tag, format!($($arg)*).as_str());
    );
}

#[macro_export]
macro_rules! infof {
    ($dst:expr, $tag:tt, $($arg:tt)*) => (
        $dst.info($tag, format!($($arg)*).as_str());
    );
}

#[macro_export]
macro_rules! warnf {
    ($dst:expr, $tag:tt, $($arg:tt)*) => (
        $dst.warn($tag, format!($($arg)*).as_str());
    );
}

#[macro_export]
macro_rules! errorf {
    ($dst:expr, $tag:tt, $($arg:tt)*) => (
        $dst.error($tag, format!($($arg)*).as_str());
    );
}


