use core::convert::TryFrom;

pub trait Configurable {
    type CfgType;
    type ErrType;
}

pub trait TryConfig<C, E> = Configurable::<CfgType=C, ErrType=E> + TryFrom<C>;