#![feature(peekable_next_if)]
#![feature(proc_macro_diagnostic)]
#![feature(extend_one)]

extern crate proc_macro;

use proc_macro::{Diagnostic, Level, TokenStream};
use proc_macro2::Span;
use core::iter::{Iterator, Peekable};
use quote::{format_ident, quote};
use std::collections::HashSet;
use syn::{AttrStyle, Attribute, AttributeArgs, ExprType, Fields, FnArg, Ident, Item, ItemFn, ItemMod, 
        ItemStatic, ItemStruct, Lit, Meta, MetaNameValue, NestedMeta, ReturnType, Stmt, Type, TypeParam, 
        Visibility, parse, parse_quote, parse_macro_input, parse_str, spanned::Spanned};

/// Marks a function as the application main function to be called on program start.
/// The function must have the signature `[unsafe] fn() -> !`.
///
/// Example:
///
/// ```rust
/// #[app_main]
/// fn main() -> ! {
///     // your app code here    
/// }
/// ```
#[proc_macro_attribute]
pub fn app_main(args: TokenStream, input: TokenStream) -> TokenStream {
    let mut f = parse_macro_input!(input as ItemFn);

    // check the function signature
    let valid_signature = f.sig.constness.is_none()
        && f.vis == Visibility::Inherited
        && f.sig.abi.is_none()
        && f.sig.inputs.is_empty()
        && f.sig.generics.params.is_empty()
        && f.sig.generics.where_clause.is_none()
        && f.sig.variadic.is_none()
        && match f.sig.output {
            ReturnType::Default => false,
            ReturnType::Type(_, ref ty) => match **ty {
                Type::Never(_) => true,
                _ => false,
            },
        };

    if !valid_signature {
        return parse::Error::new(
            f.span(),
            "`#[app_main]` function must have signature `[unsafe] fn() -> !`",
        )
        .to_compile_error()
        .into();
    }

    // TODO: add argument to specify the export name of the function 
    if !args.is_empty() {
        return parse::Error::new(Span::call_site(), "This attribute accepts no arguments")
            .to_compile_error()
            .into();
    }

    let (statics, stmts) = match extract_static_muts(f.block.stmts) {
        Err(e) => return e.to_compile_error().into(),
        Ok(x) => x,
    };

    f.sig.ident = Ident::new(
        &format!("__xtensa_lx_rt_{}", f.sig.ident),
        Span::call_site(),
    );
    f.sig.inputs.extend(statics.iter().map(|statik| {
        let ident = &statik.ident;
        let ty = &statik.ty;
        let attrs = &statik.attrs;

        // Note that we use an explicit `'static` lifetime for the entry point arguments. This makes
        // it more flexible, and is sound here, since the entry will not be called again, ever.
        syn::parse::<FnArg>(
            quote!(#[allow(non_snake_case)] #(#attrs)* #ident: &'static mut #ty).into(),
        )
        .unwrap()
    }));
    f.block.stmts = stmts;

    let tramp_ident = Ident::new(&format!("{}_trampoline", f.sig.ident), Span::call_site());
    let ident = &f.sig.ident;

    let resource_args = statics
        .iter()
        .map(|statik| {
            let (ref cfgs, ref attrs) = extract_cfgs(statik.attrs.clone());
            let ident = &statik.ident;
            let ty = &statik.ty;
            let expr = &statik.expr;
            quote! {
                #(#cfgs)*
                {
                    #(#attrs)*
                    static mut #ident: #ty = #expr;
                    &mut #ident
                }
            }
        })
        .collect::<Vec<_>>();

    if let Err(error) = check_attr_whitelist(&f.attrs, WhiteListCaller::Entry) {
        return error;
    }

    let (ref cfgs, ref attrs) = extract_cfgs(f.attrs.clone());

    quote!(
        #(#cfgs)*
        #(#attrs)*
        #[doc(hidden)]
        #[export_name = "app_main"]
        pub unsafe extern "C" fn #tramp_ident() {
            #ident(
                #(#resource_args),*
            )
        }

        #[inline(always)]
        #f
    )
    .into()
}

/// Extracts `static mut` vars from the beginning of the given statements
fn extract_static_muts(
    stmts: impl IntoIterator<Item = Stmt>,
) -> Result<(Vec<ItemStatic>, Vec<Stmt>), parse::Error> {
    let mut istmts = stmts.into_iter();

    let mut seen = HashSet::new();
    let mut statics = vec![];
    let mut stmts = vec![];
    while let Some(stmt) = istmts.next() {
        match stmt {
            Stmt::Item(Item::Static(var)) => {
                if var.mutability.is_some() {
                    if seen.contains(&var.ident) {
                        return Err(parse::Error::new(
                            var.ident.span(),
                            format!("the name `{}` is defined multiple times", var.ident),
                        ));
                    }

                    seen.insert(var.ident.clone());
                    statics.push(var);
                } else {
                    stmts.push(Stmt::Item(Item::Static(var)));
                }
            }
            _ => {
                stmts.push(stmt);
                break;
            }
        }
    }

    stmts.extend(istmts);

    Ok((statics, stmts))
}

fn extract_cfgs(attrs: Vec<Attribute>) -> (Vec<Attribute>, Vec<Attribute>) {
    let mut cfgs = vec![];
    let mut not_cfgs = vec![];

    for attr in attrs {
        if eq(&attr, "cfg") {
            cfgs.push(attr);
        } else {
            not_cfgs.push(attr);
        }
    }

    (cfgs, not_cfgs)
}

enum WhiteListCaller {
    Entry,
    Exception,
    Interrupt,
    PreInit,
}

fn check_attr_whitelist(attrs: &[Attribute], caller: WhiteListCaller) -> Result<(), TokenStream> {
    let whitelist = &[
        "doc",
        "link_section",
        "cfg",
        "allow",
        "warn",
        "deny",
        "forbid",
        "cold",
        "ram",
        "peripheral",
    ];

    'o: for attr in attrs {
        for val in whitelist {
            if eq(&attr, &val) {
                continue 'o;
            }
        }

        let err_str = match caller {
            WhiteListCaller::Entry => {
                "this attribute is not allowed on a xtensa-lx-rt entry point"
            }
            WhiteListCaller::Exception => {
                "this attribute is not allowed on an exception handler controlled by xtensa-lx-rt"
            }
            WhiteListCaller::Interrupt => {
                if eq(&attr, "naked") {
                    continue 'o;
                }

                "this attribute is not allowed on an interrupt handler controlled by xtensa-lx-rt"
            }
            WhiteListCaller::PreInit => {
                "this attribute is not allowed on a pre-init controlled by xtensa-lx-rt"
            }
        };

        return Err(parse::Error::new(attr.span(), &err_str)
            .to_compile_error()
            .into());
    }

    Ok(())
}

/// Returns `true` if `attr.path` matches `name`
fn eq(attr: &Attribute, name: &str) -> bool {
    attr.style == AttrStyle::Outer && attr.path.is_ident(name)
}


const ATTR_CONFIGURABLE_ARG_NUM: usize = 0;
const GENERIC_MIN_NUM: usize = 2;

#[proc_macro_attribute]
pub fn configurable(args: TokenStream, input: TokenStream) -> TokenStream { 
    let item_struct = parse_macro_input!(input as ItemStruct);
    let name = &item_struct.ident;
    let generics = &item_struct.generics;
    let vis = &item_struct.vis;
    let attrs = &item_struct.attrs;
    
    let args = parse_macro_input!(args as AttributeArgs);
    let len = args.len();

    if len != ATTR_CONFIGURABLE_ARG_NUM {
        return parse::Error::new(Span::call_site(), format!("The `#[configurable]` macro requires {} argument(s), but found {}. Try e.g. `#[configurable(some::ConfigStruct)]`", ATTR_CONFIGURABLE_ARG_NUM, len))
            .to_compile_error()
            .into();
    }
    
    let mut it = generics.type_params().into_iter();
    let cfg_type = it.next();
    let err_type = it.next();

    if cfg_type.is_none() || err_type.is_none() {
        return parse::Error::new(Span::call_site(), format!("The type {} needs at least {} generic type arguments", name, GENERIC_MIN_NUM))
        .to_compile_error()
        .into();
    }

    let fields = match item_struct.fields {
        Fields::Named(f) => {
            let result = quote! {
                config: #cfg_type,
                #f
            };
            Some(result)
        },
        Fields::Unnamed(_) => None,
        Fields::Unit => None
    };

    let marker = quote! {
        impl Configurable for #name #generics {
            type CfgType = #cfg_type;
            type ErrType = #err_type;
        }
    };

    if let Some(f) = fields {
        quote!(
            #(#attrs)*
            #vis struct #name #generics {
                 #f
            }

            #marker
        ).into()
        
    } else {
        quote!(
            #(#attrs)*
            #vis struct #name #generics {}
            
            #marker
        ).into()

    }

}

macro_rules! diag {
    ($level:path, $s:literal ) => {
        proc_macro::Diagnostic::new($level, $s).emit()
    };
    ( $level:path, $s:literal, $( $arg:expr ),+) => {
        proc_macro::Diagnostic::new($level, format!($s, format_args!($($arg)* ))).emit()
    };
}

#[allow(unused_macros)]
macro_rules! e {
    ( $s:literal ) => { diag!(proc_macro::Level::Error, $s, $($arg),* ) };
    ( $s:literal, $($arg:expr),* ) => { diag!(proc_macro::Level::Error, $s, $($arg),* ) };
}

#[allow(unused_macros)]
macro_rules! w {
    ( $s:literal ) => { diag!(proc_macro::Level::Error, $s, $($arg),* ) };
    ( $s:literal, $($arg:expr),* ) => { diag!(proc_macro::Level::Warning, $s, $($arg),* ) };
}

#[allow(unused_macros)]
macro_rules! n {
    ( $s:literal ) => { diag!(proc_macro::Level::Error, $s, $($arg),* ) };
    ( $s:literal, $($arg:expr),* ) => { diag!(proc_macro::Level::Note, $s, $($arg),* ) };
}

#[allow(unused_macros)]
macro_rules! h {
    ( $s:literal ) => { diag!(proc_macro::Level::Error, $s, $($arg),* ) };
    ( $s:literal, $($arg:expr),* ) => { diag!(proc_macro::Level::Help, $s, $($arg),* ) };
}

const ATTR_CONFIGURE_PERIPHERALS_MIN_ARGS: usize = 1;


#[proc_macro_attribute]
pub fn peripheral(args: TokenStream, input: TokenStream) -> TokenStream {
    let item_mod = parse_macro_input!(input as ItemMod);
    let args = parse_macro_input!(args as AttributeArgs);
    let mut arg_iter = args.into_iter().peekable();

    let mod_name = item_mod.ident.clone();
    let peripheral_mod : syn::Path = parse_quote!(target::#mod_name);

    let peripheral = if let Some(arg) = arg_iter.peek() {
        match arg {
            NestedMeta::Meta(m) => {
                match m {
                    Meta::Path(p) => {
                        Some(p)
                    },
                    _ => None,
                }
            },
            _ => None,
        }
    } else {
        None
    };

    if peripheral.is_none() {
        return parse::Error::new(Span::call_site()
            , format!("The `#[peripheral]` macro requires at least 1 argument(s), but found 0. Try e.g. `#[peripherals[target::UART0]]`"))
            .to_compile_error()
            .into();
    }

    
    let peripheral = peripheral.unwrap();
    let ident = if let Some(i) = peripheral.get_ident() {
        i
    } else {
        &peripheral.segments.last().unwrap().ident
    };

    Diagnostic::new(Level::Note, format!("peripheral ident:\n{:?}", ident)).emit();

    let mod_contents = item_mod.content.map_or(Vec::new(), |c| c.1 );
    let attrs = &item_mod.attrs;
    let from_imps = create_conversion_impls(ident, peripheral.clone());
    let mod_vis = item_mod.vis.clone();

    quote!( 
        #(#attrs)*
        #mod_vis mod #mod_name {
            #(
                #mod_contents
            )*

            impl super::BlockRef for #peripheral {
                type Output = *const #peripheral_mod::RegisterBlock;
                fn block_ref(&self) -> Self::Output {
                    #peripheral::ptr()           
                }
            }

            #(
                #from_imps
            )*
        }
    ).into()
}

fn create_conversion_impls(ident: &Ident, p: syn::Path) -> Vec<syn::Item> {

    let mut items = Vec::new();

    // match syn::parse::<syn::Item>( quote!(
                        
    //     impl<T> From<T> for #ident<T> 
    //     where T: Named + BlockRef {
    //         fn from(other: T) -> Self {
    //             let ptr = *other.block_ref();
    //             #ident{
    //                 data: ptr,
    //                 phantom: marker::PhantomData,
    //             }
    //         }
    //     }

    // ).into()) {
    //     Err(e) => {
    //         Diagnostic::new(Level::Error
    //             , format!("Could not implement From for type {:}: {:?}"
    //             , ident
    //             , e )
    //         ).emit()
    //     },
    //     Ok(x) => items.push(x),
    // }

    items
}

/// This macro allows the user to configure one or more peripherals and bind them with 
///  either in source code or device config file
///
/// 
/// Example:
/// 
///
///
///
///
///
#[proc_macro_attribute]
pub fn configure_peripherals(args: TokenStream, input: TokenStream) -> TokenStream { 
    let item_mod = parse_macro_input!(input as ItemMod);
    let mut mod_content = item_mod.content.map_or(Vec::new(), |c| c.1 );
    
    let args = parse_macro_input!(args as AttributeArgs);
    let len = args.len();

    if len < ATTR_CONFIGURE_PERIPHERALS_MIN_ARGS {
        return parse::Error::new(Span::call_site()
        , format!("The `#[configure_peripherals]` macro requires at least{} argument(s), but found {}. Try e.g. `#[configure_peripherals[target::UART0]]`", ATTR_CONFIGURE_PERIPHERALS_MIN_ARGS, len))
        .to_compile_error()
        .into();
    }
    
    let mut arg_iter = args.iter().peekable();
    let mut cfg_file;
    
    while let Some(arg) = read_next_named_arg(&mut arg_iter) {
        let (key, value) = arg;
        
        cfg_file = match key.as_str() {
            "use_file" => {
                Some(value)
            },
            _ => None,
        };
    }    
    
    let attrs = &item_mod.attrs;
    let (ref cfgs, ref attrs) = extract_cfgs(attrs.clone());
    let mod_vis = item_mod.vis.clone();
    let mod_ident = item_mod.ident.clone();
    let mut peripheral_impls: Vec<Item> = Vec::new();

    
    peripheral_impls.push(parse_quote! {
        use core::{marker, ops::Deref, fmt::Display};
    });
    for arg in arg_iter {
        // let pimpl = create_impls_from_arg(arg);
        let pimpl = create_enum_from_arg(arg);
        let pitems = pimpl.into_iter().map(|p| -> syn::Item { p.to_owned() });
        peripheral_impls.extend(pitems.into_iter());
        
    }

    let module = quote! {
        #(#cfgs)*
        #(#attrs)*
        #mod_vis mod #mod_ident {
            #(#mod_content)*

            #(#peripheral_impls)*
        }
    };

    module.into()

}

use core::slice::Iter;

fn read_next_named_arg(arg_iter: &mut Peekable<Iter<NestedMeta>>) -> Option<(String, String)> {

    let named_arg = if let Some(arg) = arg_iter.peek() {
        match arg {
            NestedMeta::Meta(m) => {
                match m {
                    Meta::NameValue(name_value) => {
                        // Some((name_value.lit.))
                        if let Some(ident) = name_value.path.get_ident() {
                            let name = format!("{}", ident.to_string());
                            let value = name_value.lit.clone();

                            if let Lit::Str(s) = value {
                                let value = format!("{}", s.value());
                                Some((name, value))    
                            } else {
                                None
                            }
                        } else {
                            None
                        }

                    },
                    _ => None
                }
            },
            _ => None,
        }
    } else {
        None
    };

    let _ = arg_iter.next_if(|&arg| named_arg.is_some());
    named_arg

}

use quote::ToTokens;
use syn::punctuated::Punctuated;
use syn::{
    Variant, 
    token::Comma,
};

fn create_enum_from_arg(arg: &NestedMeta) -> Vec<Item> {
    let arg = arg.to_owned();
    let mut items = Vec::new();
    
    match arg {
        NestedMeta::Meta(m) => {
            match m {
                Meta::List(ml) => {
                    let enum_name = ml.path.to_token_stream().to_string();
                    
                    let mut args = ml.nested.into_iter();
                    let type_arg = args.next();
                    let type_arg = match type_arg {
                        Some(ty) => {
                            match ty {
                                NestedMeta::Meta(nm) => {
                                    match nm {
                                        Meta::Path(p) => Some(p),
                                        _ => None,
                                    }
                                },
                                _ => None,
                            }
                        },
                        _ => None,
                    };
                    
                    if let None = type_arg {
                        Diagnostic::new(Level::Error, format!("Peripheral group '{:}' is missing 'type_arg' parameter", enum_name)).emit();
                        return items;
                    }

                    let type_arg = type_arg.unwrap();

                    let mut variants: Punctuated<Variant, Comma> = Punctuated::new();
                    for nested in args {

                        
                        let p = match nested {
                            NestedMeta::Meta(nm) => {
                                match nm {
                                    Meta::Path(p) => p,
                                    _ => {
                                        Diagnostic::new(Level::Error, format!("Peripheral group '{:}' must at least define 1 target struct", enum_name)).emit();
                                        continue;
                                    },
                                }
                            },
                            _ => {
                                Diagnostic::new(Level::Error, format!("Expected a peripheral group")).emit();
                                continue;
                            },
                        };
                        
                        let ident = if let Some(i) = p.get_ident() {
                            i
                        } else {
                            &p.segments.last().unwrap().ident
                        };
                        let variant: syn::Variant = parse_quote! {
                            #ident (#p)
                        };
                        variants.push(variant);               
                    }
                    
                    let phantom: syn::Variant = parse_quote! {
                        _Phantom (marker::PhantomData<T>)
                    };
                    variants.push(phantom);               
                    
                    let enum_ident = Ident::new(enum_name.as_str(), Span::call_site());
                    let item: syn::Item = parse_quote! {
                        pub enum #enum_ident<T> {
                            #variants
                        }
                    };
                    Diagnostic::new(Level::Note, format!("enum:\n{:}", item.to_token_stream().to_string())).emit();
                    items.push(item);

                    let item: syn::Item = parse_quote! {
                        impl<T: #type_arg> #enum_ident<T> {
                            pub fn unwrap() -> Box<dyn #type_arg> {

                            }
                        }
                    };
                    Diagnostic::new(Level::Note, format!("enum impl:\n{:}", item.to_token_stream().to_string())).emit();
                    items.push(item);

                },
                _ => {},
            }
        },
        
        NestedMeta::Lit(_) => {}
    }

    items
}

fn create_impls_from_arg(arg: &NestedMeta) -> Vec<Item> {
    let arg = arg.to_owned();
    let mut items = Vec::new();
    
    match arg {
        NestedMeta::Meta(m) => {
            match m {
                Meta::List(ml) => {
                    let mod_name = ml.path.to_token_stream().to_string();
                    
                    let mut args = ml.nested.into_iter();
                    let nested = args.next().unwrap();
                    
                    let p = match nested {
                        NestedMeta::Meta(nm) => {
                            Diagnostic::new(Level::Note, format!("target struct:\n{:?}", nm)).emit();
                            match nm {
                                Meta::Path(p) => p,
                                _ => {
                                    Diagnostic::new(Level::Error, format!("Peripheral group '{:}' must at least define 1 target struct", mod_name)).emit();
                                    return items;
                                },
                            }
                        },
                        _ => {
                            Diagnostic::new(Level::Error, format!("Expected a peripheral group")).emit();
                            return items;
                        },
                    };

                    let ident = if let Some(i) = p.get_ident() {
                        i
                    } else {
                        &p.segments.last().unwrap().ident
                    };
                    Diagnostic::new(Level::Note, format!("ident:\n{:}", ident)).emit();
                    
                    let local_ident = format_ident!("_{}", ident);
                    let item: syn::Item = parse_quote! {
                        pub struct #ident<T: Named + BlockRef>{
                            data: *const T,
                            phantom: marker::PhantomData<*const T>,
                        }
                    };
                    items.push(item);
                    
                    let mut register_block = p.segments.clone();
                    register_block.last_mut().unwrap().ident = syn::Ident::new(mod_name.as_str(), Span::call_site());
                    register_block.push(syn::PathSegment{ ident: syn::Ident::new("RegisterBlock", Span::call_site()), arguments: syn::PathArguments::None });
                    
                    let item: syn::Item = parse_quote! {
                        impl BlockRef for #p {
                            type Output = *const #register_block;
                            fn block_ref(&self) -> Self::Output {
                                #p::ptr()           
                            }
                        }
                    };
                    items.push(item);

                    // let item: syn::Item = parse_quote! {
                    //     impl BlockRef for #ident<#p> {
                    //         type Output = *const #register_block;
                    //         fn block_ref(&self) -> Self::Output {
                    //             #p::ptr()           
                    //         }
                    //     }
                    // };
                    // items.push(item);
                    
                    items.extend(create_named_impls(p.clone(), ident.clone()).into_iter());

                    
                    
                    // items.extend(create_copy_impls().into_iter());

                    // let item: syn::Item = parse_quote! {
                        
                    //     impl Display for #ident<#p> {

                    //         fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                    //             let name = self.name();
                    //             write!(f, "{}", name)
                    //         }
                    //     }
                        
                    // };
                    // items.push(item);

                },
                _ => {},
            }
        },
        
        NestedMeta::Lit(_) => {}
    }

    items
}


fn create_named_impls(p: syn::Path, ident: Ident) -> Vec<Item> {

    let mut items = Vec::new();

    let port_name = ident.to_string();
    let item: syn::Item = parse_quote! {
        
        impl Named for #p {
            fn name(&self) -> String {
                #port_name.to_string()
            }
        }
        
    };
    items.push(item);

    let item: syn::Item = parse_quote! {
        
        impl Named for #ident<#p> {
            fn name(&self) -> String {
                #port_name.to_string()
            }
        }
        
    };
    items.push(item);

    items

}
